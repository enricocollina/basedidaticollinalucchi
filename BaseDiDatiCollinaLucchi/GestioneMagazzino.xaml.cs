﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.EntityFrameworkCore;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per GestioneMagazzino.xaml
    /// </summary>
    public partial class GestioneMagazzino : Window
    {
        DBCOSMETICSContext _context;
        public GestioneMagazzino()
        {
            InitializeComponent();
            _context = new DBCOSMETICSContext();

            var areePerFiltro = _context.AreeStoccaggio.Select(x => new
            {
                x.Sigla,
                x.Nome
            });

            foreach (var area in areePerFiltro)
            {
                Cmb_Aree.Items.Add(new ComboBoxItem
                {
                    Content = string.Format("[{0}] - {1}", area.Sigla, area.Nome),
                    DataContext = area.Sigla,
                });
            }

        }

        private void Btn_Cerca_Click(object sender, RoutedEventArgs e)
        {
            var areaSelezionata = ((ComboBoxItem)Cmb_Aree.SelectedItem).DataContext.ToString();
            var filtroUdc = Txt_Udc.Text;

            var res = _context.UnitaCarico.ToList();
            /*
            var result = (from udc in _context.UnitaCarico

                          join pos in _context.Posizioni
                          on new { a = udc.Anno, c = udc.CodiceProgressivo }
                          equals new { a = pos.Anno.Value, c = pos.CodiceProgressivo.Value }

                          join area in _context.AreeStoccaggio
                          on new { sigla = pos.Sigla }
                          equals new { sigla = area.Sigla }

                          select new
                          {
                              udc.Anno,
                              udc.CodiceProgressivo,
                              pos.X,
                              pos.Y,
                              pos.Sigla,
                              AreaSigla = area.Sigla
                          });


            if (!string.IsNullOrWhiteSpace(areaSelezionata))
            {
                result = result.Where(x => x.AreaSigla == areaSelezionata);
            }
            if (!string.IsNullOrWhiteSpace(filtroUdc))
            {
                result = result.Where(x => x.CodiceProgressivo.ToString().Contains(filtroUdc));
            }


            var udcResult = result.ToList();
            */

            foreach (var u in res)
            {
                Lst_Udc.Items.Add(new ListBoxItem
                {
                    //Content = string.Format("{0}/{1} \t Area:{2} \t Scaffale:{3} \t Posizione:{4},{5}", u.Anno, u.CodiceProgressivo, u.AreaSigla, u.Sigla, u.X, u.Y),
                    Content = string.Format("{0}/{1}", u.Anno, u.CodiceProgressivo),

                    DataContext = new Udc
                    {
                        Anno = (int)u.Anno,
                        Numero = (int)u.CodiceProgressivo
                    }
                });
            }
        }

        private void Btn_Movimenta_Click(object sender, RoutedEventArgs e)
        {
            var udc = (Udc)((ListBoxItem)Lst_Udc.SelectedItem).DataContext;
            if (udc != null)
            {
                var window = new Movimenta(udc);
                window.Show();
            }
        }

        private void Btn_Rimuovi_Click(object sender, RoutedEventArgs e)
        {
            var udc = (Udc)((ListBoxItem)Lst_Udc.SelectedItem).DataContext;
            if(udc != null)
            {
                var UdcDaRimuovere = _context.UnitaCarico.Include(x => x.UnitaProduzione).Include(x=> x.Posizioni).Single(x => x.Anno == udc.Anno && x.CodiceProgressivo == udc.Numero);

                _context.UnitaProduzione.Remove(UdcDaRimuovere.UnitaProduzione);

                UdcDaRimuovere.Posizioni.Anno = null;
                UdcDaRimuovere.Posizioni.CodiceProgressivo = null;

                _context.UnitaCarico.Remove(UdcDaRimuovere);

                _context.SaveChanges();
            }
        }

        private void Btn_Crea_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
