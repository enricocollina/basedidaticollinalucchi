﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class FermiProduzione
    {
        public decimal Idturno { get; set; }
        public DateTime Timestamp { get; set; }
        public string Causa { get; set; }
        public string Note { get; set; }

        public virtual Turni IdturnoNavigation { get; set; }
    }
}
