﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Scaffali
    {
        public Scaffali()
        {
            Posizioni = new HashSet<Posizioni>();
        }

        public string SiglaAreaStoccaggio { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        public virtual AreeStoccaggio SiglaAreaStoccaggioNavigation { get; set; }
        public virtual ICollection<Posizioni> Posizioni { get; set; }
    }
}
