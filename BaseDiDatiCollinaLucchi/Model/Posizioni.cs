﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Posizioni
    {
        public string SiglaAreaStoccaggio { get; set; }
        public string Sigla { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public decimal? Anno { get; set; }
        public decimal? CodiceProgressivo { get; set; }

        public virtual Scaffali SiglaNavigation { get; set; }
        public virtual UnitaCarico UnitaCarico { get; set; }
    }
}
