﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class UnitaCarico
    {
        public decimal Anno { get; set; }
        public decimal CodiceProgressivo { get; set; }
        public string Barcode { get; set; }
        public string StatoQualitativo { get; set; }
        public DateTime DataScadenza { get; set; }
        public decimal Strati { get; set; }
        public decimal QuantitàStrato { get; set; }
        public decimal CodiceProdotto { get; set; }

        public virtual Prodotti CodiceProdottoNavigation { get; set; }
        public virtual Conformazioni Conformazioni { get; set; }
        public virtual Posizioni Posizioni { get; set; }
        public virtual UnitaProduzione UnitaProduzione { get; set; }
    }
}
