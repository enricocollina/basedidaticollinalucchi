﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class CicliProduzione
    {
        public CicliProduzione()
        {
            UnitaProduzione = new HashSet<UnitaProduzione>();
        }

        public string Id { get; set; }
        public decimal Idturno { get; set; }
        public DateTime? CheckIn { get; set; }
        public DateTime? CheckOut { get; set; }
        public decimal Idreparto { get; set; }
        public decimal Idricetta { get; set; }

        public virtual Reparti IdrepartoNavigation { get; set; }
        public virtual Ricette IdricettaNavigation { get; set; }
        public virtual Turni IdturnoNavigation { get; set; }
        public virtual ICollection<UnitaProduzione> UnitaProduzione { get; set; }
    }
}
