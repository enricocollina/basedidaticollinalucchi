﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class ConformazioniProdotti
    {
        public decimal Id { get; set; }
        public decimal CodiceProdotto { get; set; }
        public decimal Strati { get; set; }
        public decimal QuantitàStrato { get; set; }

        public virtual Prodotti CodiceProdottoNavigation { get; set; }
        public virtual Conformazioni Conformazioni { get; set; }
    }
}
