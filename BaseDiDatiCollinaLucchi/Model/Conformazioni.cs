﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Conformazioni
    {
        public Conformazioni()
        {
            ConformazioniProdotti = new HashSet<ConformazioniProdotti>();
            UnitaCarico = new HashSet<UnitaCarico>();
        }

        public decimal Strati { get; set; }
        public decimal QuantitàStrato { get; set; }

        public virtual ICollection<ConformazioniProdotti> ConformazioniProdotti { get; set; }
        public virtual ICollection<UnitaCarico> UnitaCarico { get; set; }
    }
}
