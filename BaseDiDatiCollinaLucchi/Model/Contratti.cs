﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Contratti
    {
        public decimal Id { get; set; }
        public string UsernamePersona { get; set; }
        public string Tipo { get; set; }
        public DateTime DataInizio { get; set; }
        public DateTime DataScadenza { get; set; }
        public float Salario { get; set; }
        public decimal OreSettimanali { get; set; }

        public virtual Persone UsernamePersonaNavigation { get; set; }
    }
}
