﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Turni
    {
        public Turni()
        {
            FermiProduzione = new HashSet<FermiProduzione>();
            TurniPersone = new HashSet<TurniPersone>();
        }

        public decimal Id { get; set; }
        public DateTime Data { get; set; }
        public string StatoAttività { get; set; }
        public decimal IdturnoFisso { get; set; }

        public virtual TurniFissi IdturnoFissoNavigation { get; set; }
        public virtual CicliProduzione CicliProduzione { get; set; }
        public virtual ICollection<FermiProduzione> FermiProduzione { get; set; }
        public virtual ICollection<TurniPersone> TurniPersone { get; set; }
    }
}
