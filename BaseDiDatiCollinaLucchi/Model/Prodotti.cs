﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Prodotti
    {
        public Prodotti()
        {
            ConformazioniProdotti = new HashSet<ConformazioniProdotti>();
            Ingredienti = new HashSet<Ingredienti>();
            UnitaCarico = new HashSet<UnitaCarico>();
            UnitaProduzione = new HashSet<UnitaProduzione>();
        }

        public string Tipo { get; set; }
        public decimal CodiceProdotto { get; set; }
        public string Descrizione { get; set; }
        public string Nome { get; set; }
        public string Conservazione { get; set; }

        public virtual Ricette Ricette { get; set; }
        public virtual ICollection<ConformazioniProdotti> ConformazioniProdotti { get; set; }
        public virtual ICollection<Ingredienti> Ingredienti { get; set; }
        public virtual ICollection<UnitaCarico> UnitaCarico { get; set; }
        public virtual ICollection<UnitaProduzione> UnitaProduzione { get; set; }
    }
}
