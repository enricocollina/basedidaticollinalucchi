﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class TitoliStudio
    {
        public string UsernamePersona { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public string Titolo { get; set; }
        public string Ente { get; set; }
        public DateTime DataConseguimento { get; set; }

        public virtual Persone UsernamePersonaNavigation { get; set; }
    }
}
