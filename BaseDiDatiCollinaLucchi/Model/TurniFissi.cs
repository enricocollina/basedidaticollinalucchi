﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class TurniFissi
    {
        public TurniFissi()
        {
            Turni = new HashSet<Turni>();
        }

        public decimal Id { get; set; }
        public DateTime OrarioInizio { get; set; }
        public DateTime OrarioFine { get; set; }

        public virtual ICollection<Turni> Turni { get; set; }
    }
}
