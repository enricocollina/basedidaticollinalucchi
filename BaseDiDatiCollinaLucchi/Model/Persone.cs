﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Persone
    {
        public Persone()
        {
            TitoliStudio = new HashSet<TitoliStudio>();
            TurniPersone = new HashSet<TurniPersone>();
        }

        public string Username { get; set; }
        public string NumDocumento { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime DataNascita { get; set; }
        public string CodiceFiscale { get; set; }
        public string Cellulare { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Paese { get; set; }
        public string Regione { get; set; }
        public string Provincia { get; set; }
        public string Via { get; set; }
        public string Civico { get; set; }
        public decimal Cap { get; set; }
        public decimal Categoria { get; set; }

        public virtual Contratti Contratti { get; set; }
        public virtual Reparti Reparti { get; set; }
        public virtual ICollection<TitoliStudio> TitoliStudio { get; set; }
        public virtual ICollection<TurniPersone> TurniPersone { get; set; }
    }
}
