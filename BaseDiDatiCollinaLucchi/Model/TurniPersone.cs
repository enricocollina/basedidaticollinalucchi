﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class TurniPersone
    {
        public string Id { get; set; }
        public decimal Idturno { get; set; }
        public string UsernamePersona { get; set; }

        public virtual Turni IdturnoNavigation { get; set; }
        public virtual Persone UsernamePersonaNavigation { get; set; }
    }
}
