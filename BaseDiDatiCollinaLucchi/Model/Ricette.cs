﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Ricette
    {
        public Ricette()
        {
            CicliProduzione = new HashSet<CicliProduzione>();
            Ingredienti = new HashSet<Ingredienti>();
        }

        public decimal Id { get; set; }
        public decimal CodiceProdotto { get; set; }
        public string Descrizione { get; set; }
        public string Nome { get; set; }

        public virtual Prodotti CodiceProdottoNavigation { get; set; }
        public virtual ICollection<CicliProduzione> CicliProduzione { get; set; }
        public virtual ICollection<Ingredienti> Ingredienti { get; set; }
    }
}
