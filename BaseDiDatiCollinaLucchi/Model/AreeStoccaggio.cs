﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class AreeStoccaggio
    {
        public AreeStoccaggio()
        {
            Scaffali = new HashSet<Scaffali>();
        }

        public string Sigla { get; set; }
        public string Nome { get; set; }
        public decimal? Capienza { get; set; }
        public decimal? Temperatura { get; set; }

        public virtual ICollection<Scaffali> Scaffali { get; set; }
    }
}
