﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class AreeProduzione
    {
        public AreeProduzione()
        {
            Reparti = new HashSet<Reparti>();
        }

        public string Sigla { get; set; }
        public string Nome { get; set; }

        public virtual ICollection<Reparti> Reparti { get; set; }
    }
}
