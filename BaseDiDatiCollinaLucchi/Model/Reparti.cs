﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Reparti
    {
        public Reparti()
        {
            CicliProduzione = new HashSet<CicliProduzione>();
        }

        public decimal Id { get; set; }
        public string UsernamePersona { get; set; }
        public string Nome { get; set; }
        public string SiglaAreaProduzione { get; set; }

        public virtual AreeProduzione SiglaAreaProduzioneNavigation { get; set; }
        public virtual Persone UsernamePersonaNavigation { get; set; }
        public virtual ICollection<CicliProduzione> CicliProduzione { get; set; }
    }
}
