﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class DBCOSMETICSContext : DbContext
    {
        public DBCOSMETICSContext()
        {
        }

        public DBCOSMETICSContext(DbContextOptions<DBCOSMETICSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AreeProduzione> AreeProduzione { get; set; }
        public virtual DbSet<AreeStoccaggio> AreeStoccaggio { get; set; }
        public virtual DbSet<CicliProduzione> CicliProduzione { get; set; }
        public virtual DbSet<Conformazioni> Conformazioni { get; set; }
        public virtual DbSet<ConformazioniProdotti> ConformazioniProdotti { get; set; }
        public virtual DbSet<Contratti> Contratti { get; set; }
        public virtual DbSet<FermiProduzione> FermiProduzione { get; set; }
        public virtual DbSet<Ingredienti> Ingredienti { get; set; }
        public virtual DbSet<Persone> Persone { get; set; }
        public virtual DbSet<Posizioni> Posizioni { get; set; }
        public virtual DbSet<Prodotti> Prodotti { get; set; }
        public virtual DbSet<Reparti> Reparti { get; set; }
        public virtual DbSet<Ricette> Ricette { get; set; }
        public virtual DbSet<Scaffali> Scaffali { get; set; }
        public virtual DbSet<TitoliStudio> TitoliStudio { get; set; }
        public virtual DbSet<Turni> Turni { get; set; }
        public virtual DbSet<TurniFissi> TurniFissi { get; set; }
        public virtual DbSet<TurniPersone> TurniPersone { get; set; }
        public virtual DbSet<UnitaCarico> UnitaCarico { get; set; }
        public virtual DbSet<UnitaProduzione> UnitaProduzione { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //Connevtion strings
                //optionsBuilder.UseSqlServer("Server=enrico\\mssqlserver01;Database=DBCOSMETICS;Trusted_Connection=True;");
                optionsBuilder.UseSqlServer("Server=pc-matteo\\sqlexpress;Database=DBCOSMETICS;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<AreeProduzione>(entity =>
            {
                entity.HasKey(e => e.Sigla)
                    .HasName("IDAREE_PRODUZIONE_ID");

                entity.ToTable("AREE_PRODUZIONE");

                entity.Property(e => e.Sigla)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AreeStoccaggio>(entity =>
            {
                entity.HasKey(e => e.Sigla)
                    .HasName("IDAREE_STOCCAGGIO_ID");

                entity.ToTable("AREE_STOCCAGGIO");

                entity.Property(e => e.Sigla)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Capienza).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Temperatura).HasColumnType("numeric(10, 0)");
            });

            modelBuilder.Entity<CicliProduzione>(entity =>
            {
                entity.ToTable("CICLI_PRODUZIONE");

                entity.HasIndex(e => e.Idturno)
                    .HasName("FKsvolgimento_ID")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CheckIn).HasColumnType("date");

                entity.Property(e => e.CheckOut).HasColumnType("date");

                entity.Property(e => e.Idreparto)
                    .HasColumnName("IDReparto")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Idricetta)
                    .HasColumnName("IDRicetta")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Idturno)
                    .HasColumnName("IDTurno")
                    .HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.IdrepartoNavigation)
                    .WithMany(p => p.CicliProduzione)
                    .HasForeignKey(d => d.Idreparto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKesecuzione");

                entity.HasOne(d => d.IdricettaNavigation)
                    .WithMany(p => p.CicliProduzione)
                    .HasForeignKey(d => d.Idricetta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKutilizzo");

                entity.HasOne(d => d.IdturnoNavigation)
                    .WithOne(p => p.CicliProduzione)
                    .HasForeignKey<CicliProduzione>(d => d.Idturno)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKsvolgimento_FK");
            });

            modelBuilder.Entity<Conformazioni>(entity =>
            {
                entity.HasKey(e => new { e.Strati, e.QuantitàStrato })
                    .HasName("IDCONFORMAZIONE_ID");

                entity.ToTable("CONFORMAZIONI");

                entity.Property(e => e.Strati).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.QuantitàStrato).HasColumnType("numeric(10, 0)");
            });

            modelBuilder.Entity<ConformazioniProdotti>(entity =>
            {
                entity.ToTable("CONFORMAZIONI_PRODOTTI");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.QuantitàStrato).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Strati).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.CodiceProdottoNavigation)
                    .WithMany(p => p.ConformazioniProdotti)
                    .HasForeignKey(d => d.CodiceProdotto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKassemblaggio");

                entity.HasOne(d => d.Conformazioni)
                    .WithMany(p => p.ConformazioniProdotti)
                    .HasForeignKey(d => new { d.Strati, d.QuantitàStrato })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKadozione");
            });

            modelBuilder.Entity<Contratti>(entity =>
            {
                entity.ToTable("CONTRATTI");

                entity.HasIndex(e => e.UsernamePersona)
                    .HasName("FKaccorda_A_ID")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.DataInizio).HasColumnType("date");

                entity.Property(e => e.DataScadenza).HasColumnType("date");

                entity.Property(e => e.OreSettimanali).HasColumnType("numeric(2, 0)");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernamePersona)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernamePersonaNavigation)
                    .WithOne(p => p.Contratti)
                    .HasForeignKey<Contratti>(d => d.UsernamePersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKaccorda_A_FK");
            });

            modelBuilder.Entity<FermiProduzione>(entity =>
            {
                entity.HasKey(e => new { e.Idturno, e.Timestamp })
                    .HasName("IDFERMO_PRODUZIONE");

                entity.ToTable("FERMI_PRODUZIONE");

                entity.Property(e => e.Idturno)
                    .HasColumnName("IDTurno")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Timestamp).HasColumnType("date");

                entity.Property(e => e.Causa)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdturnoNavigation)
                    .WithMany(p => p.FermiProduzione)
                    .HasForeignKey(d => d.Idturno)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKavvenimento");
            });

            modelBuilder.Entity<Ingredienti>(entity =>
            {
                entity.ToTable("INGREDIENTI");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Idricetta)
                    .HasColumnName("IDRicetta")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Quantità).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.CodiceProdottoNavigation)
                    .WithMany(p => p.Ingredienti)
                    .HasForeignKey(d => d.CodiceProdotto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKessenza");

                entity.HasOne(d => d.IdricettaNavigation)
                    .WithMany(p => p.Ingredienti)
                    .HasForeignKey(d => d.Idricetta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKconsumazione");
            });

            modelBuilder.Entity<Persone>(entity =>
            {
                entity.HasKey(e => e.Username)
                    .HasName("IDPERSONA_ID");

                entity.ToTable("PERSONE");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Cap)
                    .HasColumnName("CAP")
                    .HasColumnType("numeric(5, 0)");

                entity.Property(e => e.Categoria).HasColumnType("numeric(2, 0)");

                entity.Property(e => e.Cellulare)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Civico)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CodiceFiscale)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataNascita).HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumDocumento)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Paese)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Provincia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Regione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Via)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Posizioni>(entity =>
            {
                entity.HasKey(e => new { e.SiglaAreaStoccaggio, e.Sigla, e.X, e.Y })
                    .HasName("IDPOSIZIONE");

                entity.ToTable("POSIZIONI");

                entity.HasIndex(e => new { e.Anno, e.CodiceProgressivo })
                    .HasName("FKcontenitore_ID")
                    .IsUnique();

                entity.Property(e => e.SiglaAreaStoccaggio)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Sigla)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.X).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Y).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Anno).HasColumnType("numeric(10,0)");

                entity.Property(e => e.CodiceProgressivo).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.UnitaCarico)
                    .WithOne(p => p.Posizioni)
                    .HasForeignKey<Posizioni>(d => new { d.Anno, d.CodiceProgressivo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKcontenitore_FK");

                entity.HasOne(d => d.SiglaNavigation)
                    .WithMany(p => p.Posizioni)
                    .HasForeignKey(d => new { d.SiglaAreaStoccaggio, d.Sigla })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKsuddivisione");
            });

            modelBuilder.Entity<Prodotti>(entity =>
            {
                entity.HasKey(e => e.CodiceProdotto)
                    .HasName("IDPRODOTTI_ID");

                entity.ToTable("PRODOTTI");

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Conservazione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Reparti>(entity =>
            {
                entity.ToTable("REPARTI");

                entity.HasIndex(e => e.UsernamePersona)
                    .HasName("FKsupervisione_ID")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaAreaProduzione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernamePersona)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.SiglaAreaProduzioneNavigation)
                    .WithMany(p => p.Reparti)
                    .HasForeignKey(d => d.SiglaAreaProduzione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKlocalità");

                entity.HasOne(d => d.UsernamePersonaNavigation)
                    .WithOne(p => p.Reparti)
                    .HasForeignKey<Reparti>(d => d.UsernamePersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKsupervisione_FK");
            });

            modelBuilder.Entity<Ricette>(entity =>
            {
                entity.ToTable("RICETTE");

                entity.HasIndex(e => e.CodiceProdotto)
                    .HasName("FKcreazione_ID")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodiceProdottoNavigation)
                    .WithOne(p => p.Ricette)
                    .HasForeignKey<Ricette>(d => d.CodiceProdotto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKcreazione_FK");
            });

            modelBuilder.Entity<Scaffali>(entity =>
            {
                entity.HasKey(e => new { e.SiglaAreaStoccaggio, e.Sigla })
                    .HasName("IDSCAFFALE_ID");

                entity.ToTable("SCAFFALI");

                entity.Property(e => e.SiglaAreaStoccaggio)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Sigla)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.SiglaAreaStoccaggioNavigation)
                    .WithMany(p => p.Scaffali)
                    .HasForeignKey(d => d.SiglaAreaStoccaggio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKarredamento");
            });

            modelBuilder.Entity<TitoliStudio>(entity =>
            {
                entity.HasKey(e => new { e.UsernamePersona, e.Titolo })
                    .HasName("IDTITOLO_STUDIO");

                entity.ToTable("TITOLI_STUDIO");

                entity.Property(e => e.UsernamePersona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataConseguimento).HasColumnType("date");

                entity.Property(e => e.Ente)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernamePersonaNavigation)
                    .WithMany(p => p.TitoliStudio)
                    .HasForeignKey(d => d.UsernamePersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKconsegue_A");
            });

            modelBuilder.Entity<Turni>(entity =>
            {
                entity.ToTable("TURNI");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Data).HasColumnType("date");

                entity.Property(e => e.IdturnoFisso)
                    .HasColumnName("IDTurnoFisso")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.StatoAttività)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdturnoFissoNavigation)
                    .WithMany(p => p.Turni)
                    .HasForeignKey(d => d.IdturnoFisso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKessere");
            });

            modelBuilder.Entity<TurniFissi>(entity =>
            {
                entity.ToTable("TURNI_FISSI");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.OrarioFine).HasColumnType("date");

                entity.Property(e => e.OrarioInizio).HasColumnType("date");
            });

            modelBuilder.Entity<TurniPersone>(entity =>
            {
                entity.ToTable("TURNI_PERSONE");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Idturno)
                    .HasColumnName("IDTurno")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.UsernamePersona)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdturnoNavigation)
                    .WithMany(p => p.TurniPersone)
                    .HasForeignKey(d => d.Idturno)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKscomposizione");

                entity.HasOne(d => d.UsernamePersonaNavigation)
                    .WithMany(p => p.TurniPersone)
                    .HasForeignKey(d => d.UsernamePersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKlavoro");
            });

            modelBuilder.Entity<UnitaCarico>(entity =>
            {
                entity.HasKey(e => new { e.Anno, e.CodiceProgressivo })
                    .HasName("IDUNIT_LOAD_ID");

                entity.ToTable("UNITA_CARICO");

                entity.Property(e => e.Anno).HasColumnType("numeric(10,0)");

                entity.Property(e => e.CodiceProgressivo).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Barcode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.DataScadenza).HasColumnType("date");

                entity.Property(e => e.QuantitàStrato).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.StatoQualitativo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Strati).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.CodiceProdottoNavigation)
                    .WithMany(p => p.UnitaCarico)
                    .HasForeignKey(d => d.CodiceProdotto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKcomposizione");

                entity.HasOne(d => d.Conformazioni)
                    .WithMany(p => p.UnitaCarico)
                    .HasForeignKey(d => new { d.Strati, d.QuantitàStrato })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKdisposizione");
            });

            modelBuilder.Entity<UnitaProduzione>(entity =>
            {
                entity.ToTable("UNITA_PRODUZIONE");

                entity.HasIndex(e => new { e.Anno, e.CodiceProgressivo })
                    .HasName("FKconcatenazione_ID")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Anno).HasColumnType("numeric(10,0)");

                entity.Property(e => e.CodiceProdotto).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CodiceProgressivo).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.IdcicloProduzione)
                    .IsRequired()
                    .HasColumnName("IDCicloProduzione")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Lotto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.QuantitàEffettiva).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.CodiceProdottoNavigation)
                    .WithMany(p => p.UnitaProduzione)
                    .HasForeignKey(d => d.CodiceProdotto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKgenerazione");

                entity.HasOne(d => d.IdcicloProduzioneNavigation)
                    .WithMany(p => p.UnitaProduzione)
                    .HasForeignKey(d => d.IdcicloProduzione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKproduzione");

                entity.HasOne(d => d.UnitaCarico)
                    .WithOne(p => p.UnitaProduzione)
                    .HasForeignKey<UnitaProduzione>(d => new { d.Anno, d.CodiceProgressivo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKconcatenazione_FK");
            });
        }
    }
}
