﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class UnitaProduzione
    {
        public decimal Id { get; set; }
        public decimal? Anno { get; set; }
        public decimal? CodiceProgressivo { get; set; }
        public decimal QuantitàEffettiva { get; set; }
        public string Lotto { get; set; }
        public string IdcicloProduzione { get; set; }
        public decimal CodiceProdotto { get; set; }

        public virtual Prodotti CodiceProdottoNavigation { get; set; }
        public virtual CicliProduzione IdcicloProduzioneNavigation { get; set; }
        public virtual UnitaCarico UnitaCarico { get; set; }
    }
}
