﻿using System;
using System.Collections.Generic;

namespace Model.BaseDiDatiCollinaLucchi
{
    public partial class Ingredienti
    {
        public string Id { get; set; }
        public decimal Quantità { get; set; }
        public decimal Idricetta { get; set; }
        public decimal CodiceProdotto { get; set; }

        public virtual Prodotti CodiceProdottoNavigation { get; set; }
        public virtual Ricette IdricettaNavigation { get; set; }
    }
}
