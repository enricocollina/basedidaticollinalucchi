﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model.BaseDiDatiCollinaLucchi;
using BaseDiDatiCollinaLucchi.Resources;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        DBCOSMETICSContext _Context;
        public Login()
        {
            InitializeComponent();
            _Context = new DBCOSMETICSContext();
        }

        private void Btn_Login_Click(object sender, RoutedEventArgs e)
        {
            Lbl_ErrPassword.Content = string.Empty;
            Lbl_ErrUsername.Content = string.Empty;

            var username = Txt_Username.Text;
            var password = Txt_Password.Password;

            if (string.IsNullOrWhiteSpace(username))
            {
                Lbl_ErrUsername.Content = Shared.Err_EmptyValue;
                Lbl_ErrUsername.Foreground = Brushes.Red;
                return;
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                Lbl_ErrPassword.Content = Shared.Err_EmptyValue;
                Lbl_ErrPassword.Foreground = Brushes.Red;
                return;
            }

            var utente = _Context.Persone.SingleOrDefault(x => x.Username == username);
            if (utente == null)
            {
                Lbl_ErrUsername.Content = string.Format(Shared.Err_Username, username);
                Lbl_ErrUsername.Foreground = Brushes.Red;
                return;
            }

            if(utente.Password != password)
            {
                Lbl_ErrPassword.Content = string.Format(Shared.Err_Password, username);
                Lbl_ErrPassword.Foreground = Brushes.Red;
                return;
            }

            Menu window = new Menu(utente.Username, (int)utente.Categoria);
            window.Show();

            Close();
        }
    }
}
