﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.EntityFrameworkCore;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per CicloDiProduzione.xaml
    /// </summary>
    public partial class CicloDiProduzione : Window
    {
        DBCOSMETICSContext _context;
        public CicloDiProduzione(string id)
        {
            InitializeComponent();
            _context = new DBCOSMETICSContext();

            var cicloDiProduzione = _context.CicliProduzione.Single(x => x.Id == id);

            var ricetta = _context.Ricette.Single(x => x.Id == cicloDiProduzione.Idricetta);

            var ingredienti = _context.Ingredienti.Include(x => x.CodiceProdottoNavigation).Where(x => x.Idricetta == ricetta.Id)
                .Select(x => new
                {
                    x.Quantità,
                    x.CodiceProdottoNavigation.Nome
                });

            var operai = _context.TurniPersone.Include(x => x.UsernamePersonaNavigation).Where(x => x.Idturno == cicloDiProduzione.Idturno)
                .Select(x => new
                {
                    x.UsernamePersonaNavigation.Nome,
                    x.UsernamePersonaNavigation.Cognome
                });

            foreach (var ingrediente in ingredienti)
            {
                Lst_Ingredienti.Items.Add(new ListBoxItem
                {
                    Content = string.Format("{0} => {1}Kg", ingrediente.Nome, ingrediente.Quantità)
                });
            }

            Lst_Ricetta.Items.Add(new ListBoxItem
            {
                Content = string.Format("{0}{1}", ricetta.Id, ricetta.Nome)
            });

            foreach (var operaio in operai)
            {
                Lst_Operai.Items.Add(new ListBoxItem
                {
                    Content = string.Format("{0} {1}", operaio.Nome, operaio.Cognome)
                });
            }

        }

        private void Btn_Fermo_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
