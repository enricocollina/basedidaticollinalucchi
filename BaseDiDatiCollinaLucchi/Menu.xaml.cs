﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        DBCOSMETICSContext _context;
        string _username;
        int _type;
        public Menu(string username, int type)
        {
            InitializeComponent();
            _context = new DBCOSMETICSContext();
            _type = type;
            _username = username;

            InitializeMenu();
        }
        private void InitializeMenu()
        {
            if(_type == (int)Enum.Roles.Amministratore)
            {
                return;
            }
            if(_type == (int)Enum.Roles.Amministratore)
            {
                Btn_ElencoDipendenti.IsEnabled = false;
                return;
            }
            if(_type == (int)Enum.Roles.Operaio)
            {
                Btn_ElencoDipendenti.IsEnabled = false;
                Btn_GestioneMagazzino.IsEnabled = false;
                Btn_GestioneTurni.IsEnabled = false;
            }
        }

        private void Btn_GestioneTurni_Click(object sender, RoutedEventArgs e)
        {
            //var window = new GestioneTurni();
            //window.Show();
        }

        private void Btn_GestioneMagazzino_Click(object sender, RoutedEventArgs e)
        {
            var window = new GestioneMagazzino();
            window.Show();
        }

        private void Btn_ElencoDipendenti_Click(object sender, RoutedEventArgs e)
        {
            var window = new ElencoDipendenti();
            window.Show();
        }

        private void btn_CicloProduzione_Click(object sender, RoutedEventArgs e)
        {
            var window = new CicloDiProduzione("1");
            window.Show();
        }

        private void Btn_MieiTurni_Click(object sender, RoutedEventArgs e)
        {
            var window = new IMieiTurni("Nerone_55");
            window.Show();
        }
    }
}
