﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDiDatiCollinaLucchi.Enum
{
    public enum Roles
    {
        Amministratore = 1,

        CapoReparto = 2,

        Operaio = 3
    }
}
