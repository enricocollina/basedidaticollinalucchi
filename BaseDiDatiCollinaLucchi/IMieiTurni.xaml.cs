﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per IMieiTurni.xaml
    /// </summary>
    public partial class IMieiTurni : Window
    {
        DBCOSMETICSContext _context;

        public IMieiTurni(string username)
        {
            var oggi = DateTime.Today;
            var giorniALunedi = ((int)DayOfWeek.Monday - (int)oggi.DayOfWeek + 7) % 7;
            DateTime prossimoLunedi = oggi.AddDays(giorniALunedi);

            InitializeComponent();
            _context = new DBCOSMETICSContext();

            var turni = (from turno in _context.Turni

                         join turnoFisso in _context.TurniFissi
                         on new { t = turno.IdturnoFisso }
                         equals new { t = turnoFisso.Id }

                         join turnoPersona in _context.TurniPersone
                         on new { t = turno.Id }
                         equals new { t = turnoPersona.Idturno }

                         where turno.Data >= oggi && turno.Data <= prossimoLunedi

                         select new
                         {
                             turno.Id,
                             turno.Data,
                             turnoFisso.OrarioInizio,
                             turnoFisso.OrarioFine,
                             turno.StatoAttività
                         }).ToList();


            foreach (var turno in turni)
            {
                Lst_TurniSettimanali.Items.Add(new ListBoxItem
                {
                    Content = string.Format("{0}\t{1}\t{2}", turno.Data.ToLongDateString(), turno.OrarioInizio.TimeOfDay, turno.OrarioFine.TimeOfDay)
                });
            }

        }

        private void Lst_TurniSettimanali_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
