﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per ElencoDipendenti.xaml
    /// </summary>
    public partial class ElencoDipendenti : Window
    {
        DBCOSMETICSContext _Context;

        public ElencoDipendenti()
        {
            _Context = new DBCOSMETICSContext();
            InitializeComponent();

            Btn_Info.IsEnabled = false;

            var persone = _Context.Persone.ToList();

            foreach (var p in persone)
            {
                LstBox_Dipendenti.Items.Add(new ListBoxItem
                {
                    Content = p.Nome + '\t' + p.Cognome + "\t\t\t" + p.Username + "\t\t\t" + p.Email + "\t\t" + p.Cellulare + "\t\t\t" + p.Categoria,
                    DataContext = p.Username
                });
            }
        }

        private void Btn_Aggiungi_Click(object sender, RoutedEventArgs e)
        {
            var persona = new Persone()
            {
                NumDocumento = "333",
                Username = "JonnyPonny00",
                Nome = "Jony",
                Cognome = "Luciano",
                DataNascita = DateTime.Now,
                CodiceFiscale = "JNNYPNNY6754LCC",
                Cellulare = "3456787612",
                Email = "jonnyponny@gmailcom",
                Password = "1234",
                Paese = "Italia",
                Regione = "Lazio",
                Provincia = "Roma",
                Via = "Aureliana",
                Civico = "344D",
                Cap = 23444,
                Categoria = 2
            };

            _Context.Persone.Add(persona);
            _Context.SaveChanges();


        }

        private void LstBox_Dipendenti_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Btn_Info.IsEnabled = true;

        }

        private void Btn_Info_Click(object sender, RoutedEventArgs e)
        {
            string persona_tofind = (string)((ListBoxItem)LstBox_Dipendenti.SelectedItem).DataContext;

            var result = (from usr in _Context.Persone
                          join tsd in _Context.TitoliStudio
                          on usr.Username equals tsd.UsernamePersona
                          where usr.Username == persona_tofind
                          join cnt in _Context.Contratti
                          on usr.Username equals cnt.UsernamePersona
                          where usr.Username == persona_tofind
                          select new
                          {
                              TipoTitolo = tsd.Tipo,
                              Titolo = tsd.Titolo,
                              TipoContratto = cnt.Tipo,
                              Salario = cnt.Salario
                          }).SingleOrDefault();
            if (result != null)
                Lbl_Info.Content = string.Format("{0}, {1}, {2}, {3}", result.Titolo, result.TipoTitolo, result.TipoContratto, result.Salario);
        }

        private void Btn_Rimuovi_Click(object sender, RoutedEventArgs e)
        {
            var persona_to_delette = _Context.Persone.Where(x => x == LstBox_Dipendenti.DataContext);
            _Context.Persone.Remove((Persone)persona_to_delette);

            _Context.SaveChanges();
        }

        private void Btn_Modifica_Click(object sender, RoutedEventArgs e)
        {
            //selected_persona = (Persone)LstBox_Dipendenti.DataContext;
        }
    }
}
