﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BaseDiDatiCollinaLucchi.Resources;
using Model.BaseDiDatiCollinaLucchi;

namespace BaseDiDatiCollinaLucchi
{
    /// <summary>
    /// Logica di interazione per Movimenta.xaml
    /// </summary>
    public partial class Movimenta : Window
    {
        DBCOSMETICSContext _context;
        Udc udcDaMovimentare;
        public Movimenta(Udc udc)
        {
            udcDaMovimentare = udc;
            InitializeComponent();

            _context = new DBCOSMETICSContext();

            Lbl_Udc.Content = string.Format("{0}/{1}", udc.Anno, udc.Numero);

            var udcInGiacenza = _context.UnitaCarico.Where(x => x.Anno == udc.Anno && x.CodiceProgressivo == udc.Numero);

            var posizioneUdc = (from u in udcInGiacenza

                                join pos in _context.Posizioni
                                on new { a = u.Anno, c = u.CodiceProgressivo }
                                equals new { a = pos.Anno.Value, c = pos.CodiceProgressivo.Value }

                                join area in _context.AreeStoccaggio
                                on new { sigla = pos.Sigla }
                                equals new { sigla = area.Sigla }

                                select new
                                {
                                    pos.X,
                                    pos.Y,
                                    pos.Sigla,
                                    AreaSigla = area.Sigla
                                }).SingleOrDefault();

            Lbl_Posizione.Content = string.Format("Area: {0}  Scaffale: {1}  Posizione: {2},{3}", posizioneUdc.AreaSigla, posizioneUdc.Sigla, posizioneUdc.X, posizioneUdc.Y);

            var aree = _context.AreeStoccaggio.Select(x => new
            {
                x.Sigla,
                x.Nome
            }).ToList();

            foreach (var area in aree)
            {
                Cmb_Area.Items.Add(new ComboBoxItem
                {
                    Content = string.Format("[{0}] - {1}", area.Sigla, area.Nome),
                    DataContext = area.Sigla
                });
            }
        }

        private void Cmb_Area_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Cmb_Scaffale.Items.Clear();
            Cmb_Scaffale.SelectedIndex = -1;

            Cmb_Locazione.Items.Clear();
            Cmb_Locazione.SelectedIndex = -1;

            var areaSelezionata = ((ComboBoxItem)Cmb_Area.SelectedItem).DataContext.ToString();

            var scaffali = _context.Scaffali.Where(x => x.SiglaAreaStoccaggio == areaSelezionata).ToList();

            foreach (var scaffale in scaffali)
            {
                Cmb_Area.Items.Add(new ComboBoxItem
                {
                    Content = string.Format("[{0}] - {1}", scaffale.Sigla, scaffale.Nome),
                    DataContext = scaffale.Sigla
                });
            }

        }

        private void Cmb_Scaffale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Cmb_Locazione.Items.Clear();
            Cmb_Locazione.SelectedIndex = -1;

            var scaffaleSelezionato = ((ComboBoxItem)Cmb_Scaffale.SelectedItem).DataContext.ToString();

            var locazioniLibere = _context.Posizioni.Where(x => x.SiglaAreaStoccaggio == scaffaleSelezionato && x.Anno == null && x.CodiceProgressivo == null).ToList();

            foreach (var locazione in locazioniLibere)
            {
                Cmb_Area.Items.Add(new ComboBoxItem
                {
                    Content = string.Format("({0},{1})", locazione.X, locazione.Y),
                    DataContext = locazione
                });
            }
        }

        private void Btn_Salva_Click(object sender, RoutedEventArgs e)
        {
            Lbl_Error.Content = string.Empty;

            if (Cmb_Area.SelectedItem == null || Cmb_Scaffale.SelectedItem == null || Cmb_Locazione.SelectedItem == null)
            {
                Lbl_Error.Content = Shared.Err_NotAllFieldCompiled;
                Lbl_Error.Foreground = Brushes.Red;
                return;
            }

            var locazione = (Posizioni)((ComboBoxItem)Cmb_Locazione.SelectedItem).DataContext;

            var posizioneOld = _context.Posizioni.Where(x => x.Anno == udcDaMovimentare.Anno && x.CodiceProgressivo == udcDaMovimentare.Numero).Single();

            posizioneOld.Anno = null;
            posizioneOld.CodiceProgressivo = null;

            locazione.Anno = udcDaMovimentare.Anno;
            locazione.CodiceProgressivo = udcDaMovimentare.Numero;

            _context.SaveChanges();
        }
    }
}
