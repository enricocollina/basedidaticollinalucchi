﻿-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Mon Jun 17 18:17:07 2019 
-- * LUN file: C:\Users\Matteo\Desktop\UNIVERSITA\IN CORSO\BASI DI DATI\Progetto\DB-Project.lun 
-- * Schema: DB_Logic/1-1-1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database DBCOSMETICS;


-- DBSpace Section
-- _______________


-- Tables Section
-- _____________ 

create table PERSONE (
     Username varchar(50) not null,
     NumDocumento varchar(9) not null,
     Nome varchar(50) not null,
     Cognome varchar(50) not null,
     DataNascita date not null,
     CodiceFiscale varchar(16) not null,
     Cellulare varchar(10) not null,
     Email varchar(50) not null,
     Password varchar(50) not null,
     Paese varchar(50) not null,
     Regione varchar(50) not null,
     Provincia varchar(50) not null,
     Via varchar(50) not null,
     Civico varchar(10) not null,
     CAP numeric(5) not null,
     Categoria numeric(2) not null,
     constraint IDPERSONA_ID primary key (Username));

create table AREE_PRODUZIONE (
     Sigla varchar(50) not null,
     Nome varchar(50) not null,
     constraint IDAREE_PRODUZIONE_ID primary key (Sigla));

create table AREE_STOCCAGGIO (
     Sigla varchar(10) not null,
     Nome varchar(50) not null,
     Capienza numeric(10),
     Temperatura numeric(10),
     constraint IDAREE_STOCCAGGIO_ID primary key (Sigla));

create table CICLI_PRODUZIONE (
     ID varchar(10) not null,
     IDTurno numeric(10) not null,
     CheckIn date,
     CheckOut date,
     IDReparto numeric(10) not null,
     IDRicetta numeric(10) not null,
     constraint IDCICLO_PRODUZIONE_ID primary key (ID),
     constraint FKsvolgimento_ID unique (IDTurno));

create table CONFORMAZIONI (
     Strati numeric(10) not null,
     QuantitàStrato numeric(10) not null,
     constraint IDCONFORMAZIONE_ID primary key (Strati, QuantitàStrato));

create table CONTRATTI (
     ID numeric(10) not null,
     UsernamePersona varchar(50) not null,
     Tipo varchar(50) not null,
     DataInizio date not null,
     DataScadenza date not null,
     Salario float(10) not null,
     OreSettimanali numeric(2) not null,
     constraint IDCONTRATTO primary key (ID),
     constraint FKaccorda_A_ID unique (UsernamePersona));

create table FERMI_PRODUZIONE (
     IDTurno numeric(10) not null,
     Timestamp date not null,
     Causa varchar(50) not null,
     Note varchar(250),
     constraint IDFERMO_PRODUZIONE primary key (IDTurno, Timestamp));

create table INGREDIENTI (
     ID char(10) not null,
     Quantità numeric(10) not null,
     IDRicetta numeric(10) not null,
     CodiceProdotto numeric(10) not null,
     constraint IDINGREDIENTI primary key (ID));

create table CONFORMAZIONI_PRODOTTI (
     ID numeric(10)  not null,
     CodiceProdotto numeric(10) not null,
     Strati numeric(10) not null,
     QuantitàStrato numeric(10) not null,
     constraint IDCONFORMAZIONI_PRODOTTO primary key (ID));

create table POSIZIONI (
     SiglaAreaStoccaggio varchar(10) not null,
     Sigla varchar(10) not null,
     X numeric(10) not null,
     Y numeric(10) not null,
     Anno numeric(10) not null,
     CodiceProgressivo numeric(10) not null,
     constraint IDPOSIZIONE primary key (SiglaAreaStoccaggio, Sigla, X, Y),
     constraint FKcontenitore_ID unique (Anno, CodiceProgressivo));

create table PRODOTTI (
     Tipo varchar(50) not null,
     CodiceProdotto numeric(10)  not null,
     Descrizione varchar(250) not null,
     Nome varchar(50) not null,
     Conservazione varchar(50) not null,
     constraint IDPRODOTTI_ID primary key (CodiceProdotto));

create table REPARTI (
     ID numeric(10)  not null,
     UsernamePersona varchar(50) not null,
     Nome varchar(50) not null,
     SiglaAreaProduzione varchar(50) not null,
     constraint IDREPARTO primary key (ID),
     constraint FKsupervisione_ID unique (UsernamePersona));

create table RICETTE (
     ID numeric(10)  not null,
     CodiceProdotto numeric(10) not null,
     Descrizione varchar(250) not null,
     Nome varchar(50) not null,
     constraint IDRICETTE_ID primary key (ID),
     constraint FKcreazione_ID unique (CodiceProdotto));

create table SCAFFALI (
     SiglaAreaStoccaggio varchar(10) not null,
     Nome varchar(50) not null,
     Sigla varchar(10) not null,
     constraint IDSCAFFALE_ID primary key (SiglaAreaStoccaggio, Sigla));

create table TITOLI_STUDIO (
     UsernamePersona varchar(50) not null,
     Nome varchar(50) not null,
     Tipo varchar(50) not null,
     Titolo varchar(50) not null,
     Ente varchar(50) not null,
     DataConseguimento date not null,
     constraint IDTITOLO_STUDIO primary key (UsernamePersona, Titolo));

create table TURNI (
     ID numeric(10)  not null,
     Data date not null,
     StatoAttività char not null,
     IDTurnoFisso numeric(10) not null,
     constraint IDTURNO_ID primary key (ID));

create table TURNI_FISSI (
     ID numeric(10)  not null,
     OrarioInizio date not null,
     OrarioFine date not null,
     constraint IDTURNI_FISSI primary key (ID));

create table TURNI_PERSONE (
     ID char(10) not null,
     IDTurno numeric(10) not null,
     UsernamePersona varchar(50) not null,
     constraint IDTURNI_INDIVIDUALI primary key (ID));

create table UNITA_CARICO (
     Anno numeric(10) not null,
     CodiceProgressivo numeric(10)  not null,
     Barcode varchar(50) not null,
     StatoQualitativo varchar(50) not null,
     DataScadenza date not null,
     Strati numeric(10) not null,
     QuantitàStrato numeric(10) not null,
     CodiceProdotto numeric(10) not null,
     constraint IDUNIT_LOAD_ID primary key (Anno, CodiceProgressivo));

create table UNITA_PRODUZIONE (
     ID numeric(10)  not null,
     Anno numeric(10) not null,
     CodiceProgressivo numeric(10) not null,
     QuantitàEffettiva numeric(10) not null,
     Lotto varchar(50) not null,
     IDCicloProduzione varchar(10) not null,
     CodiceProdotto numeric(10) not null,
     constraint IDUNITA_PRODUZIONE primary key (ID),
     constraint FKconcatenazione_ID unique (Anno, CodiceProgressivo));


-- Constraints Section
-- ___________________ 


alter table CICLI_PRODUZIONE add constraint FKesecuzione
     foreign key (IDReparto)
     references REPARTI;

alter table CICLI_PRODUZIONE add constraint FKsvolgimento_FK
     foreign key (IDTurno)
     references TURNI;

alter table CICLI_PRODUZIONE add constraint FKutilizzo
     foreign key (IDRicetta)
     references RICETTE;

alter table CONTRATTI add constraint FKaccorda_A_FK
     foreign key (UsernamePersona)
     references PERSONE;

alter table FERMI_PRODUZIONE add constraint FKavvenimento
     foreign key (IDTurno)
     references TURNI;

alter table INGREDIENTI add constraint FKconsumazione
     foreign key (IDRicetta)
     references RICETTE;

alter table INGREDIENTI add constraint FKessenza
     foreign key (CodiceProdotto)
     references PRODOTTI;

alter table CONFORMAZIONI_PRODOTTI add constraint FKassemblaggio
     foreign key (CodiceProdotto)
     references PRODOTTI;

alter table CONFORMAZIONI_PRODOTTI add constraint FKadozione
     foreign key (Strati, QuantitàStrato)
     references CONFORMAZIONI;

alter table POSIZIONI add constraint FKcontenitore_FK
     foreign key (Anno, CodiceProgressivo)
     references UNITA_CARICO;

alter table POSIZIONI add constraint FKsuddivisione
     foreign key (SiglaAreaStoccaggio, Sigla)
     references SCAFFALI;

alter table REPARTI add constraint FKlocalità
     foreign key (SiglaAreaProduzione)
     references AREE_PRODUZIONE;

alter table REPARTI add constraint FKsupervisione_FK
     foreign key (UsernamePersona)
     references PERSONE;

alter table RICETTE add constraint FKcreazione_FK
     foreign key (CodiceProdotto)
     references PRODOTTI;

alter table SCAFFALI add constraint FKarredamento
     foreign key (SiglaAreaStoccaggio)
     references AREE_STOCCAGGIO;

alter table TITOLI_STUDIO add constraint FKconsegue_A
     foreign key (UsernamePersona)
     references PERSONE;

alter table TURNI add constraint FKessere
     foreign key (IDTurnoFisso)
     references TURNI_FISSI;

alter table TURNI_PERSONE add constraint FKscomposizione
     foreign key (IDTurno)
     references TURNI;

alter table TURNI_PERSONE add constraint FKlavoro
     foreign key (UsernamePersona)
     references PERSONE;

alter table UNITA_CARICO add constraint FKdisposizione
     foreign key (Strati, QuantitàStrato)
     references CONFORMAZIONI;

alter table UNITA_CARICO add constraint FKcomposizione
     foreign key (CodiceProdotto)
     references PRODOTTI;

alter table UNITA_PRODUZIONE add constraint FKproduzione
     foreign key (IDCicloProduzione)
     references CICLI_PRODUZIONE;

alter table UNITA_PRODUZIONE add constraint FKconcatenazione_FK
     foreign key (Anno, CodiceProgressivo)
     references UNITA_CARICO;

alter table UNITA_PRODUZIONE add constraint FKgenerazione
     foreign key (CodiceProdotto)
     references PRODOTTI;


-- Index Section
-- _____________ 
